//
//  searchactions
//
//  Created by Graeme Hoffman on 2015-06-11.
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

var Reflux = require('reflux');

var SearchActions = Reflux.createActions([
  'queryItunes',
  'clearSearch',
  'setActive',
  'activatePanel',
  'changeSearchQuery'
]);

module.exports = SearchActions;