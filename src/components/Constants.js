/*
 *
 * Constants
 *
 */

var Constants = {};

// keys which this panel does not respond to
Constants.IGNORE_KEYS = {
  9: "tab",
  12: "num",
  13: "enter",
  17: "ctrl",
  18: "alt",
  19: "pause",
  20: "caps",
  33: "pageup",
  34: "pagedown",
  35: "end",
  36: "home",
  37: "left",
  38: "up",
  39: "right",
  40: "down",
  44: "print",
  45: "insert",
  91: "cmd",
  92: "cmd",
  93: "cmd",
  106: "num_multiply",
  107: "num_add",
  108: "num_enter",
  109: "num_subtract",
  110: "num_decimal",
  111: "num_divide",
  112: "f1",
  113: "f2",
  114: "f3",
  115: "f4",
  116: "f5",
  117: "f6",
  118: "f7",
  119: "f8",
  120: "f9",
  121: "f10",
  122: "f11",
  123: "f12",
  124: "print",
  144: "num",
  145: "scroll",
  224: "cmd",
  225: "alt",
  57392: "ctrl",
  63289: "num"
};


module.exports = Constants;
