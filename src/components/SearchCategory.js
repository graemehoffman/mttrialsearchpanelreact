'use strict';
/*
 *
 * SearchCategory
 *
 * Display for the Search Category. Used by SearchGrid
 */

// Requires
var React = require('react');
var SearchEntry = require('./SearchEntry');
require('../styles/SearchCategory.sass');

var SearchCategory = React.createClass({
    render: function() {
      var catName = this.props.title;
      var catEntries = this.props.entries;
      var catCount = 0;
      var max = this.props.max;
      return (
        <div className="col-md-4 search-category" >
          <h3>{catName}</h3>
          {catEntries.map(function (entry) {
            catCount++;
            if (catCount>max) {
              return;
            } else{
              return(
                <SearchEntry entry={entry} key={entry.trackId} />
              );
            }
          })}
        </div>
      );
    }
});

module.exports = SearchCategory;