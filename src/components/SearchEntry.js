'use strict';
/*
 *
 * SearchEntry
 *
 * Display for individual SearchEntry. Used by SearchCategory
 */

// Requires
var React = require('react');
require('../styles/SearchEntry.sass');

var SearchEntry = React.createClass({
    render: function() {
      var entry = this.props.entry;
      return (
        <div className="search-panel-thumbnail thumbnail clearfix">
          <a href={entry.previewUrl}><img className="pull-left" src={entry.artworkUrl100} alt="{entry.trackName}" /></a>
          <div className="caption">
            <h4><a href={entry.previewUrl}>{entry.trackName}</a></h4>
            <p>{entry.artistName}</p>
          </div>
        </div>
        
      );
    }
});

module.exports = SearchEntry;