'use strict';
/*
 *
 * SearchGrid
 *
 * Main component for search return part of search panel. Rendered by SearchPanel. 
 * Splits the data into categories before display 
 *
 */


// Requires
var React = require('react');
var Reflux = require('reflux');
var SearchStore = require('../stores/SearchStore');
var SearchCategory = require('./SearchCategory');
var _ = require('lodash-node');


// TODO: make this depend on the itunes API for full listing of categories
var _kindStrings = {
  "song":"Songs",
  "feature-movie": "Feature Movies",
  "tv-episode": "TV Episodes",
  "podcast":"Podcasts",
  "podcast-episode":"Postcast Episodes",
  "music-video": "Music Videos",
  undefined:"Misc"
};


// Component parsing the search returns into grouped entries
// based on categories 
var SearchGrid = React.createClass({
    mixins: [Reflux.connect(SearchStore, 'searchData')],
    
    // extract kind name from reference
    // To Do: query itunes for full list of kinds
    getKindStringFromKey: function(key) {
      return _.get(_kindStrings, key);
    },
    
    
    render: function() {
      var maxPerCategory = this.props.maxPerCategory;
      // lodash organize in categories
      var groupedResults = _.groupBy(this.state.searchData.queryReturns, 'kind');
      // retrieve all the values of the property name 
      var categories = _.uniq(_.pluck(this.state.searchData.queryReturns, "kind"));
      // TO DO : build out row display properly
      var grid = this;
      if (this.state.searchData.loading){
        return (null);
      } else {
        if (this.state.searchData.queryReturns.length!==0) {
          // loop thru categories
          return (
            <div className="search-grid" key="search-entries">
              <div className="row">
              {categories.map(function (cat) {
                var catHeader = grid.getKindStringFromKey(cat);
                return (
                  <SearchCategory title={catHeader} key={cat} entries={groupedResults[cat]} max={maxPerCategory} />
                  );
              })}
              </div>
            </div>
          );
        } else {
          return (<div key="search-entries-empty">Bummer! No entries were found matching that search.</div>);
        }
      }
      
      
      
    }
});

module.exports = SearchGrid;

