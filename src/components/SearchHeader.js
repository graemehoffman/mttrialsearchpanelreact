'use strict';
/*
 *
 * SearchHeader
 *
 * Displays the header text (Type Anyway Search) or search button if Modernizr says we're in a touch environment. 
 */

// Requires
var React = require('react');
var Reflux = require('reflux');
var $ = require("jquery");
var SearchActions = require('../actions/SearchActions');
var SearchStore = require('../stores/SearchStore');


// Component displaying a part of the header 
var SearchHeader = React.createClass({
    // handles click on search button
    handleClick: function(e){
      SearchActions.setActive(true);
    },
    // renders this component
    render: function() {
      var isTouch = Modernizr.touch;
      // if device is touch enabled
      if(isTouch){
        return (
         <form class="navbar-form navbar-left">
            <div class="form-group">
              <a type="submit" className="btn btn-primary" id="search-activate-btn" onClick={this.handleClick}>Search</a>
            </div>
          </form>
         );
      // if device is NOT touch enabled
      } else {
        return (
            <a className="navbar-brand" href="#">Type Anywhere Search</a>
          );
      }
    }
});


module.exports = SearchHeader;

