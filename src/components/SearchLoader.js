'use strict';
/*
 *
 * SearchLoader
 *
 * Displays the loader item. This component responds to SearchStore which keeps a "loading" variable
 */

// Requires
var React = require('react/addons');
var Reflux = require('reflux');
var SearchStore = require('../stores/SearchStore');
require('../styles/SearchLoader.sass');


// component declaration
var SearchLoader = React.createClass({
    
    // connected to Seacrh Store 
    mixins: [Reflux.connect(SearchStore, 'searchData')],
    
    // Renders as visible if the searchData loading is true
    render: function() {
      var loadingClass = this.state.searchData.loading ? 'loading' : ''; 
      var cx = React.addons.classSet;
      var classes = cx('row','search-loader-row', loadingClass);
      return (
        <div className={classes}>
          <div className="col-md-12">
            <div className="search-loader-container">
               <div className="search-loader-inner">
                  Loading...
               </div>
            </div>
          </div>
        </div>
      );
    }
});

module.exports = SearchLoader;