'use strict';
/*
 *
 * SearchPanel
 *
 * Displays the full search popup or panel. Main.js opens and closes this component. 
 * User enters search terms in the input and then this query initiates the queryItunes action call
 */


// Requires
var React = require('react/addons');
var Reflux = require('reflux');
var $ = require("jquery");
var SearchActions = require('../actions/SearchActions');
var SearchStore = require('../stores/SearchStore');
var SearchGrid = require('./SearchGrid');
var SearchLoader = require('./SearchLoader');
require('normalize.css');
require('../styles/SearchPanel.sass');


// Component which renders the search panel with search results and input field
var SearchPanel = React.createClass({
    
    // connected to Seacrh Store 
    mixins: [Reflux.connect(SearchStore, 'searchData')],
    
    // focuses the inout field if its active
    // Seems not to fire until after the keydown event is finished
    componentDidUpdate: function () {
      if (this.state.searchData.active){
        this.refs.searchPanelQ.getDOMNode().focus(); 
      }
    },
    
    // initiate the query on changes to the search field
    handleKeyInput: function(event) {
      var targetValue = event.target.value;
      if (targetValue.length===0){
        SearchActions.setActive(false);
      } else if (targetValue.length===1){
        SearchActions.clearSearch();
      } else {
        SearchActions.queryItunes(targetValue);
      }
      
    },
    
    // build out the search panel
    render: function() {
      var cx = React.addons.classSet;
      var activeClass = this.state.searchData.active ? "active": "";
      var classes = cx('search-panel-container', activeClass);
      var maxPerCat = this.props.maxPerCat;
      return (
        <div id="search-panel" className={classes}>
          <div className="search-panel-inner">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <h2 className="search-header">Search Our Website</h2>
                  <h6 className="search-subheader">Start Typing</h6>
                  <input type="text" value={this.state.searchData.query} name="search-panel-q" id="search-panel-q" ref="searchPanelQ" className="search-field" onChange={this.handleKeyInput} />
                </div>
              </div>
              <SearchLoader />
              <SearchGrid maxPerCategory={maxPerCat} />
            </div>
          </div>
        </div>
      );
    }
    
    
});


module.exports = SearchPanel;
