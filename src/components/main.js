'use strict';
/*
 *
 * main
 *
 * Mainline for Search panel related functionality
 */

// require
var React = require('react');
var SearchPanel = require('./SearchPanel');
var SearchHeader = require('./SearchHeader');
var $ = require("jquery");
var Constants = require('./Constants');
var SearchActions = require('../actions/SearchActions');


// checks to see if we should ignore the Key
function shouldIgnoreKeyEvent (event) {
  var key = event.which;
  for (var keytoignore in Constants.IGNORE_KEYS) {
      if (keytoignore == key){
        return true;
      }
  }
  return false;
}


// Opens the panel and focuses the input field
// Working through reactjs loses the key input
// so we're using the jquery backup
function openPanel (){
  SearchActions.setActive(true);
  $("#search-panel").addClass("active");      // display is based on css for greater flexibility
  $('input[name="search-panel-q"]').focus();  // focus does get called before keypress so the letter gets lost 
}

// Closes the panel 
// called below
function closePanel (){
  SearchActions.setActive(false);
  $("#search-panel").removeClass("active");
  $('input[name="search-panel-q"]').blur();  // blurs the input
}



// Jquery required to capture events outside of the panel component 
$(function(){
  var $searchField = $('input[name="search-panel-q"]');
  // high level key down for to control appearance
  $(document).keydown( function(event) {
    var $focused = $(':focus');
    var currentActiveElement = document.activeElement;
    var $currentActiveElement = $(document.activeElement);
    var searchFieldHasFocus = $searchField.is(':focus');
    var key = event.which;
    
    // if the search field already have focus
    if (searchFieldHasFocus){
      // ignore certain keys 
      if (shouldIgnoreKeyEvent(event)){
        event.preventDefault();
        return;
      }
      
      // capturing BACKSPACE and DEL
      if( key === 8 || key === 46 ){
        if ($searchField.val()===""){
          //SearchActions.setActive(false);
          closePanel();
          event.preventDefault();
        }
      // capturing ESC
      } else if (key === 27) {       
        // check if ESC was hit then close
        closePanel();
      }
    } else {
      // ignoring certain keys, ESC or DEL/Backspace
      if (shouldIgnoreKeyEvent(event) || (key === 27) || (key === 8) || (key === 46) ){
        event.preventDefault();
        return;
      }
      
      // if focus is body or doc then open
      if (!$currentActiveElement || $currentActiveElement.get(0) === $("body").get(0) || $currentActiveElement.get(0) === $(document).get(0)){
        // open the search panel
        openPanel();
      }
    }
  });        

});


// init the main Search Panel
var searchpanel = document.getElementById('search-panel-container');
var maxEntryPerCategory = 5;
React.render(<SearchPanel maxPerCat={maxEntryPerCategory} />, searchpanel);

// init Search exp for touch devices
var searchheader = document.getElementById('search-touch-container');
React.render(<SearchHeader />, searchheader);



