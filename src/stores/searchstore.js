"use strict";
/*
 * Search Store
 * Handle query return data for Search Panel
 *
 * The store listens to actions triggered by SearchActions
 *
 */

// module requires
var Reflux = require('reflux');
var $ = require('jquery');
var SearchActions = require('../actions/SearchActions');
var _ = require('lodash-node');


// Hooked up toe Search Actions
// empties, queries itunes, and triggers up thru the hierarchy
var SearchStore = Reflux.createStore({
    listenables: [ SearchActions ],
    sourceUrl: 'https://itunes.apple.com/search',
    requestCount:0,
    
    // used by reflux to populate states upstream in the components
    getInitialState: function() {
      this.searchData= {
        loading:false,
        queryReturns: [],
        active:false,
        queryValue:""
      };
      return this.searchData;
    },
    
    // clear out the query returns
    onClearSearch: function() {
        this.updateSearchData({
          loading:false,
          queryReturns: [],
          active:true,
          queryValue:""
        });
    },
    
    // changes the activity of the app
    onSetActive: function(bool) {
      // changes the current searchData based on new active bool
      this.updateSearchData({
        loading:this.searchData.loading,
        queryReturns: this.searchData.queryReturns,
        active:bool,
        queryValue:this.searchData.queryValue
      });
    },
    
    // called whenever we change the query returns
    updateSearchData: function(newSearchData){
      this.searchData = newSearchData;
      this.trigger(this.searchData); // sends the updated list to all listening components (TodoApp)
    },
    
    
    // initiates call to itunes with new query string
    // clears out already existent query and ajax in progress
    queryItunes: function(queryString) {
      
      // used to address jsonp issue
      // abort jponp called still get called.
      this.requestCount++;
      var requestCount = this.requestCount;
      
      // show loader
      this.updateSearchData({
        loading:true,
        queryReturns: [],
        active:true,
        queryValue:this.searchData.queryValue
      });
      
      var dataType = "jsonp";
      
      // data for ajax call
      var itunesQueryData = {
        "term":queryString
      };
      // uses jsonp for easier testing locally
      this.currentQuery = $.ajax({
          url: this.sourceUrl,
          dataType: dataType,
          requestCount: this.requestCount,
          cache: false,
          data:itunesQueryData,
          context: this
      })
      .done(function(data){
        if (requestCount!==this.requestCount) {
          return;
        }
        // update search to trigger render upstream
        this.updateSearchData({
          loading:false,
          queryReturns: data.results,
          active:true
        });
      })
      .fail(function(){
        console.log("ajax fail");
        // captures both abort and internet failures
        // TODO handle 
      });
      
      
    }
});

module.exports = SearchStore;