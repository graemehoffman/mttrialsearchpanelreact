'use strict';

describe('MtTrialApp', function () {
  var React = require('react/addons');
  var MtTrialApp, component;

  beforeEach(function () {
    var container = document.createElement('div');
    container.id = 'content';
    document.body.appendChild(container);

    MtTrialApp = require('components/MtTrialApp.js');
    component = React.createElement(MtTrialApp);
  });

  it('should create a new instance of MtTrialApp', function () {
    expect(component).toBeDefined();
  });
});
